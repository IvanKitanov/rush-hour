package com.internship.rushhour.controllers;

import com.internship.rushhour.entities.User;
import com.internship.rushhour.models.UserGetModel;
import com.internship.rushhour.models.UserPostModel;
import com.internship.rushhour.services.UserServiceInterface;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserController {
    private UserServiceInterface service;
    private ModelMapper modelMapper;

    @Autowired
    public UserController(UserServiceInterface service, ModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<User> optionalUser = service.get(id);
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalUser.get(), UserGetModel.class));
        }
        return ResponseEntity.notFound()
                .build();
    }

    @GetMapping
    public HttpEntity get() {
        List<User> users = service.get();

        List<UserGetModel> userGetModels = new ArrayList<>();
        for (User user : users) {
            userGetModels.add(modelMapper.map(user, UserGetModel.class));
        }
        return ResponseEntity.ok(userGetModels);
    }

    @PostMapping
    public HttpEntity create(@RequestBody UserPostModel userPostModel) {
        User mapped = modelMapper.map(userPostModel, User.class);
        Optional<User> optionalUser = service.create(mapped);
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalUser.get(), UserGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }

    @PutMapping(value = "/{id}")
    public HttpEntity update(@PathVariable Integer id, @RequestBody UserPostModel userPostModel) {
        Optional<User> optionalUser = service.get(id);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        User mapped = modelMapper.map(userPostModel, User.class);
        mapped.setId(id);
        optionalUser = service.create(mapped);
        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalUser.get(), UserGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }

    @DeleteMapping(value = "/{id}")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<User> optionalUser = service.get(id);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        service.delete(id);
        return ResponseEntity.noContent()
                .build();
    }
}
