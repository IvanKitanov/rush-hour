package com.internship.rushhour.services;

import com.internship.rushhour.entities.User;

import java.util.List;
import java.util.Optional;

public interface UserServiceInterface {
    Optional<User> get(Integer id);
    List<User> get();
    Optional<User> create(User user);
    Optional<User> update(User user);
    void delete(Integer id);
}