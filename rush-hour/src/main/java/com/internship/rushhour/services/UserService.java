package com.internship.rushhour.services;

import com.internship.rushhour.entities.User;
import com.internship.rushhour.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements  UserServiceInterface {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<User> get(Integer id){

        return  userRepository.findById(id);
    }

    @Override
    public List<User> get(){

        return userRepository.findAll();
    }

    @Override
    public Optional<User> create(User user){

        return Optional.of(userRepository.save(user));
    }

    @Override
    public Optional<User> update(User user){

        return Optional.of(userRepository.save(user));
    }

    @Override
    public void delete(Integer id){
        userRepository.deleteById(id);
    }

}
