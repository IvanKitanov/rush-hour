package com.internship.rushhour.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AppUsers", schema = "rush-hour")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String appRole;
    public User(){
       super();

    }
    public User(String firstName, String lastName, String email, String password ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;

    }
    public String getFirstName(){

        return firstName;
    }
    public void setFirstName(String firstName){

        this.firstName = firstName;
    }
    public String getLastName(){

        return lastName;
    }
    public void setLastName(String lastName){

        this.lastName = lastName;
    }
    public String getEmail(){

        return email;
    }
    public void setEmail(String email){

        this.email = email;
    }
    public String getPassword(){

        return password;
    }
    public void setPassword(String password){

        this.password = password;
    }
    public String getAppRole(){

        return appRole;
    }
    public void String (String appRole){

        this.appRole=appRole;
    }

    public Integer getId(){

        return id;
    }
    public void setId(Integer id){

        this.id = id;
    }

}
