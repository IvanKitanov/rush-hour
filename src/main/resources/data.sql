INSERT INTO roles (name) VALUES ("ROLE_USER");
INSERT INTO roles (name) VALUES ("ROLE_ADMIN");
INSERT INTO app_users (email, first_name, last_name, password, roles_id) VALUES ("kkirilov@gmail.com","Kiril","Kirilov","$2a$10$xZosshQi1poOP7t6z7wZe.YxgKkMlmLVEDr423lzspRFcKyud2zyW",2);
INSERT INTO app_users (email, first_name, last_name, password, roles_id) VALUES ("ddimitrov@gmail.com","Dimitar","Dimitrov","$2a$10$suPUV7Ps7CgYusb5UCjwHeHZAeFnnkj9.COJQ1/dLaFTbXsooqQme",1);
INSERT INTO activities (id, duration, name, price) VALUES(1,90,"English Lessons",20.0);
INSERT INTO activities (id, duration, name, price) VALUES(2,45,"Spa procedures",45.0);