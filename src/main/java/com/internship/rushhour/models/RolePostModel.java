package com.internship.rushhour.models;

import com.internship.rushhour.entities.Role;
import com.internship.rushhour.entities.User;

import java.util.ArrayList;
import java.util.List;

public class RolePostModel {
    private String name;
    private List<User> users= new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
