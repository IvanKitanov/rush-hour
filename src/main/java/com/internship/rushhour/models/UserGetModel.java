package com.internship.rushhour.models;

import com.internship.rushhour.entities.Role;

public class UserGetModel {
    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private Role role;
    public Integer getId(){

        return id;
    }
    public void setId(Integer id) {

        this.id = id;
    }
    public String getFirstName() {

        return firstName;
    }
    public void setFirstName(String firstName){

        this.firstName = firstName;
    }
    public String getLastName() {

        return lastName;
    }
    public void setLastName(String lastName){

        this.lastName = lastName;
    }
    public String getEmail(){
        return email;
    }
    public void setEmail(String email){

        this.email = email;
    }

    public String getRole() {
        return role.getName();
    }

    public void setRole(Role role) {
        this.role = role;
    }


}
