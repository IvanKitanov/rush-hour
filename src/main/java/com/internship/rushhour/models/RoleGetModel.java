package com.internship.rushhour.models;

import com.internship.rushhour.entities.User;

import java.util.ArrayList;
import java.util.List;

public class RoleGetModel {
    private Integer id;
    private String name;
    private List<User> users= new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /*public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
    public void addUser(User user){
        users.add(user);
    }

    public void removeUser(User user){
        users.remove(user);
    }

     */
}
