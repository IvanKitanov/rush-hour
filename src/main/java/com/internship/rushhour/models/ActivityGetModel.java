package com.internship.rushhour.models;

import java.time.Duration;

public class ActivityGetModel {
    private  Integer id;
    private String name;
    private Double price;
    private Integer duration;

    public Integer getId(){

        return id;
    }
    public void setId(Integer id){

        this.id = id;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){

        this.name = name;
    }
    public Double getPrice(){
        return price;
    }
    public void setPrice(Double price){
        this.price = price;
    }
    public Integer getDuration(){
        return duration;
    }
    public void setDuration(Integer duration){

        this.duration = duration;
    }
}
