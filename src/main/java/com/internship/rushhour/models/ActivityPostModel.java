package com.internship.rushhour.models;

import java.time.Duration;

public class ActivityPostModel {
    private String name;
    private Double price;
    private Integer duration;
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price){
        this.price = price;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
