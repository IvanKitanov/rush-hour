package com.internship.rushhour.controllers;

import com.internship.rushhour.entities.Role;
import com.internship.rushhour.entities.User;
import com.internship.rushhour.payload.ApiResponse;
import com.internship.rushhour.payload.JwtAuthenticationResponse;
import com.internship.rushhour.payload.LoginRequest;
import com.internship.rushhour.payload.SignUpRequest;
import com.internship.rushhour.security.JwtTokenProvider;
import com.internship.rushhour.services.RoleService;
import com.internship.rushhour.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/auth")

public class AuthenticationController {

    private AuthenticationManager authenticationManager;
    private UserService userService;
    private RoleService roleService;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider tokenProvider;
    private ModelMapper modelMapper;

    @Autowired
    public AuthenticationController(AuthenticationManager authenticationManager, RoleService roleService, PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider,ModelMapper modelMapper, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
        this.modelMapper=modelMapper;
        this.userService=userService;
    }

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        if(userService.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        User user = modelMapper.map(signUpRequest,User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Optional<Role> optionalRole=roleService.getByName("ROLE_USER");
        if(optionalRole.isPresent()){
            Role userRole = optionalRole.get();
            user.setRole(userRole);
            Optional<User> optionalUser=userService.create(user);
            if(optionalUser.isPresent()){
                User result = optionalUser.get();
                URI location = ServletUriComponentsBuilder
                        .fromCurrentContextPath().path("/api/v1/users/email/{email}")
                        .buildAndExpand(result.getEmail()).toUri();

                return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
            }
        }
        return (ResponseEntity<?>) ResponseEntity.badRequest();
    }
}
