package com.internship.rushhour.controllers;

import com.internship.rushhour.entities.Role;
import com.internship.rushhour.models.RoleGetModel;
import com.internship.rushhour.models.RolePostModel;
import com.internship.rushhour.services.RoleServiceInterface;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api/v1/roles")
public class RoleController {
    private RoleServiceInterface service;
    private ModelMapper modelMapper;

    @Autowired
    public RoleController(RoleServiceInterface service, ModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<Role> optionalRole = service.get(id);
        if (optionalRole.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalRole.get(), RoleGetModel.class));
        }
        return ResponseEntity.notFound()
                .build();
    }

    @GetMapping(value = "/getName/{name}")
    public HttpEntity getByName(@PathVariable("name") String name) {
        Optional<Role> optionalRole = service.getByName(name);
        if (optionalRole.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalRole.get(), RoleGetModel.class));
        }
        return ResponseEntity.notFound()
                .build();
    }

    @GetMapping
    public HttpEntity get(){
        List<Role> roles=service.get();
        List<RoleGetModel> roleGetModels=new ArrayList<>();
        for(Role role: roles){
            roleGetModels.add(modelMapper.map(role, RoleGetModel.class));
        }
        return ResponseEntity.ok(roleGetModels);
    }

    @PostMapping
    public HttpEntity create(@RequestBody RolePostModel rolePostModel) {
        Role mapped= modelMapper.map(rolePostModel,Role.class);
        Optional<Role> optionalRole= service.create(mapped);
        if(optionalRole.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalRole.get(), RoleGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }

    @PutMapping(value = "/{id}")
    public HttpEntity update(@PathVariable("id") Integer id, @RequestBody RolePostModel rolePostModel) {
        Optional<Role> optionalRole = service.get(id);
        if (!optionalRole.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        Role mapped = modelMapper.map(rolePostModel, Role.class);
        mapped.setId(id);
        optionalRole = service.create(mapped);
        if (optionalRole.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalRole.get(), RoleGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }

    @DeleteMapping(value = "/{id}")
    public HttpEntity delete(@PathVariable("id") Integer id){
        Optional<Role> optionalRole=service.get(id);
        if(optionalRole.isPresent()){
            service.delete(id);
            return ResponseEntity.noContent()
                    .build();
        }
        return ResponseEntity.notFound()
                .build();
    }

}