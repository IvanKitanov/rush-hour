package com.internship.rushhour.controllers;

import com.internship.rushhour.entities.Activity;
import com.internship.rushhour.models.ActivityGetModel;
import com.internship.rushhour.models.ActivityPostModel;
import com.internship.rushhour.services.ActivityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping(value = "/api/v1/activities")
public class ActivityController {
    private ActivityService service;
    private ModelMapper modelMapper;

    @Autowired
    public ActivityController(ActivityService service, ModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }
    @GetMapping(value = "/{id}")
    public HttpEntity get(@PathVariable("id") Integer id) {
        Optional<Activity> optionalActivity = service.get(id);
        if (optionalActivity.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalActivity.get(), ActivityGetModel.class));
        }
        return ResponseEntity.notFound()
                .build();
    }

    @GetMapping
    public HttpEntity get() {
        List<Activity> activities = service.get();
        List<ActivityGetModel> toDoGetModels = new ArrayList<>();
        for (Activity activity : activities) {
            toDoGetModels.add(modelMapper.map(activity, ActivityGetModel.class));
        }
        return ResponseEntity.ok(toDoGetModels);
    }
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public HttpEntity create(@RequestBody ActivityPostModel activityPostModel) {
        Activity mapped = modelMapper.map(activityPostModel, Activity.class);
        Optional<Activity> optionalActivity = service.create(mapped);
        if (optionalActivity.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalActivity.get(), ActivityGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }
    @PutMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public HttpEntity update(@PathVariable("id") Integer id,@RequestBody ActivityPostModel activityPostModel) {
        Optional<Activity> optionalActivity = service.get(id);
        if (!optionalActivity.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        Activity mapped = modelMapper.map(activityPostModel, Activity.class);
        mapped.setId(id);
        optionalActivity = service.create(mapped);
        if (optionalActivity.isPresent()) {
            return ResponseEntity.ok(modelMapper.map(optionalActivity.get(), ActivityGetModel.class));
        }
        return ResponseEntity.unprocessableEntity()
                .build();
    }
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public HttpEntity delete(@PathVariable("id") Integer id) {
        Optional<Activity> optionalActivity = service.get(id);
        if (!optionalActivity.isPresent()) {
            return ResponseEntity.notFound()
                    .build();
        }
        service.delete(id);
        return ResponseEntity.noContent()
                .build();
    }

}

