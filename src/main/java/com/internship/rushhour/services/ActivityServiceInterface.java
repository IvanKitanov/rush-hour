package com.internship.rushhour.services;

import com.internship.rushhour.entities.Activity;

import java.util.List;
import java.util.Optional;

public interface ActivityServiceInterface {
    Optional<Activity> get(Integer id);
    List<Activity> get();
    Optional<Activity> create(Activity activity);
    Optional<Activity> update(Activity activity);
    void delete(Integer id);
}
