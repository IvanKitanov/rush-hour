package com.internship.rushhour.services;

import com.internship.rushhour.entities.Activity;
import com.internship.rushhour.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ActivityService implements  ActivityServiceInterface{
    private ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @Override
    public Optional<Activity> get(Integer id) {
        return  activityRepository.findById(id);
    }

    @Override
    public List<Activity> get() {
        return activityRepository.findAll();
    }

    @Override
    public Optional<Activity> create(Activity activity) {
        return Optional.of(activityRepository.save(activity));
    }

    @Override
    public Optional<Activity> update(Activity activity) {
        return Optional.of(activityRepository.save(activity));
    }

    @Override
    public void delete(Integer id) {
        activityRepository.deleteById(id);
    }

}
