package com.internship.rushhour.services;

import com.internship.rushhour.entities.Role;
import com.internship.rushhour.repositories.RoleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleService implements RoleServiceInterface {
    private RoleRepository roleRepository;
    private ModelMapper modelMapper;

    @Autowired
    public RoleService(RoleRepository roleRepository, ModelMapper modelMapper) {
        this.roleRepository = roleRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Optional<Role> get(Integer id) {
        return  roleRepository.findById(id);
    }

    @Override
    public List<Role> get() {
        return  roleRepository.findAll();
    }

    @Override
    public Optional<Role> create(Role role) {
        return Optional.of(roleRepository.save(role));
    }

    @Override
    public Optional<Role> update(Role role) {
        return Optional.of(roleRepository.save(role));
    }

    @Override
    public Optional<Role> getByName(String name){
        return Optional.of(roleRepository.getByName(name));
    }

    @Override
    public void delete(Integer id) {
        roleRepository.deleteById(id);
    }
}
