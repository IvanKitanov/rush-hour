package com.internship.rushhour.services;



import com.internship.rushhour.entities.Role;

import java.util.List;
import java.util.Optional;

public interface RoleServiceInterface {
    Optional<Role> get(Integer id);
    List<Role> get();
    Optional<Role> create(Role role);
    Optional<Role> update(Role role);
    void delete(Integer id);
    Optional<Role> getByName(String name);
}

