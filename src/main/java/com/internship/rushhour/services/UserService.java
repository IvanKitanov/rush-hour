package com.internship.rushhour.services;

import com.internship.rushhour.entities.Role;
import com.internship.rushhour.entities.User;
import com.internship.rushhour.repositories.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserServiceInterface {


    private UserRepository userRepository;
    private RoleService service;
    private ModelMapper modelMapper;

    @Autowired
    public UserService(UserRepository userRepository, RoleService service, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @Override
    public Optional<User> get(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> get() {

        return userRepository.findAll();
    }

    @Override
    public Optional<User> create(User user) {
        Optional<Role> optionalRole=service.getByName("ROLE_USER");
        if(optionalRole.isPresent()){
            Role role= optionalRole.get();
            user.setRole(role);
            return Optional.of(userRepository.save(user));
        }
        return null;
    }

    @Override
    public Optional<User> update(User user) {

        return Optional.of(userRepository.save(user));
    }
    @Override
    public Optional<User> promote(User user){
        Optional<Role> optionalRole=service.getByName("ROLE_ADMIN");
        if(optionalRole.isPresent()){
            Role role= optionalRole.get();
            user.setRole(role);
            return Optional.of(userRepository.save(user));
        }
        return null;
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

}

