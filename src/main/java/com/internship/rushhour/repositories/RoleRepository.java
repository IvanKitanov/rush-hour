package com.internship.rushhour.repositories;

import com.internship.rushhour.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    public Role getByName(String name);
}
